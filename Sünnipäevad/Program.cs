﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sünnipäevad
{
    class Program
    {
        // need muutujad on "näha" mõlemas meetodis - Main ja Algväärtused
        
        // siia sisse loome inimesed
        static List<Inimene> Inimesed = new List<Inimene>();
        // see on sünnipäevade vahemik, mida me arvestame
        static DateTime Lõpp = DateTime.Parse("1.1.2000");
        static DateTime Algus = DateTime.Parse("1.1.1955");
        static int period = (Lõpp - Algus).Days;

        static void Main(string[] args)
        {
            Algväärtused();

            foreach (var i in Inimesed)
                Console.WriteLine(i);

            int s = 0;
            foreach (var x in Inimesed) s += x.GetVanus();
            Console.WriteLine("Keskmine vanus on {0}", s / Inimesed.Count);

            int kaugus = int.MaxValue;
            Inimene järgmine = null;
            foreach (var kes in Inimesed)
            {
                DateTime sp;
                int i = DateTime.Now.Year - kes.Sünniaeg.Year - 1;

                //int i = 0;

                while ((sp = kes.Sünniaeg.AddYears(i++)) < DateTime.Now) ;
                
            
                if ((sp - DateTime.Now).Days < kaugus )
                {
                    järgmine = kes;
                    kaugus = (sp - DateTime.Now).Days;
                }

            }

            Console.WriteLine($"järgmine sünnipäevalaps on {järgmine.Nimi}");
            

        }

        static void Algväärtused()
        {
            string[] nimed =
            {
                "Henn",
                "Ants",
                "Peeter",
                "Joosep",
                "Jaak",
                "Kalle",
                "Malle",
                "Pille",
                "Toomas"
            };

            Random r = new Random(7);
            foreach(var x in nimed)
            {
                Inimesed.Add(new Inimene(x, Algus.AddDays(r.Next() % period)));
            }

            Inimesed.Add(new Inimene("Kahtlane", DateTime.Parse("29.2.2000")));
        }
    }

    class Inimene
    {
        public string Nimi;
        public DateTime Sünniaeg;

        public Inimene(string nimi, DateTime sünniAeg)
        {
            this.Nimi = nimi; this.Sünniaeg = sünniAeg;
        }

        public override string ToString()
        {
            return $"Inimene {Nimi} sündinud {Sünniaeg.ToShortDateString()} - vanus {GetVanus()}";
        }

        public int GetVanus()
        {
            return (DateTime.Now - this.Sünniaeg).Days / 365;
        }
    }
}
